﻿using System;

namespace skillset6
{
    class Program
    {
        public static double AddNum(double x, double y)
        {
            return x + y;
        }
        public static double SubtractNum(double x, double y)
        {
            return x - y;
        }
        public static double MultiplyNum(double x, double y)
        {
            return x * y;
        }
        public static double DivideNum(double x, double y)
        {
                return x / y;
        }
        public static double PowerNum(double x, double y)
        {
            return Math.Pow(x, y);
        }

        static void Main(string[] args)
        {
            bool redo = true; 
            string requirements =
            @"Title: Value Returning Functions
            Author: Connor Payne";
            
            Console.Write(requirements);
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\n\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:ss t"));


            do
            {
            Console.Write("\nnum1: ");
                double num1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("num2: ");
                double num2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nChoose a mathematical operation: ");

            Console.WriteLine("\n1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplicaiton");
            Console.WriteLine("4 - Division");
            Console.WriteLine("5 - Exponentiation");

            int oper = 0;
            oper = Convert.ToInt16(Console.ReadLine());

            switch(oper)
            {
                case 1:
                Console.WriteLine("\n*** Result of Addition Operation: ***");
                Console.WriteLine(AddNum(num1, num2));
                redo = true;
                    break;
                
                case 2:
                Console.WriteLine("\n*** Result of Subtraction Operation: ***");
                Console.WriteLine(SubtractNum(num1, num2));
                redo = true;
                    break;  

                case 3:
                Console.WriteLine("\n*** Result of Multiplication Operation: ***");
                Console.WriteLine(MultiplyNum(num1, num2));
                redo = true;
                    break;

                case 4:
                if (num2 == 0)
                {   
                    Console.WriteLine("Cannot divide by zero");
                    redo = false;
                    break;
                }
                Console.WriteLine("\n*** Result of Division Operation: ***");
                Console.WriteLine(DivideNum(num1, num2));
                redo = true;
                    break;

                case 5:
                Console.WriteLine("\n*** Result of Power Operation: ***");
                Console.WriteLine(PowerNum(num1, num2));
                redo = true;
                    break;
            }
            }while (redo == false);



            Console.WriteLine("\nPress Enter to exit...");
            Console.ReadKey();
        }
    }
}
