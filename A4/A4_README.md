
# LIS4369

## Connor Payne

### Assignment 4 Requirements:

*Sub-Heading:*

Program Requirements:
Using Class Inheritance 
Author: Connor Payne

1. Create Person class (must be stored in discrete file apart from Main() method)

    a. Create three protected data members:
        1.Fname
        2.Lname
        3.Age


    b. Create three setter/mutator methods:
        1.SetFname
        2.SetLname
        3.SetAge

    c. Create four getter/accessor methods:
        1.GetFname
        2.GetLname
        3.GetAge
        4.GetObjectInfo

NOTE: For this exercise, do *not* use shorthand/shortcut get/set methods.

    d. Create two constructors:
        1.default constructor (accepts no arguments)
        2.parameterized constructor that accepts three arguments 

    e. Instantiate two person objects:
        1.one from default constructor (display default values, then modify and display new data member value)
        2.one from parameterized constructor passing two arguments to its constructor's parameters), display data member values 
    
2. Create Student class (must be stored in discrete file apart from Main() method)

    a. Create three private data members:
        1.college
        2.major
        3.gpa

    b. Create three getter/accersor methods:
        1.GetName
        2.GetFullName
        3.GetObjectInfo (demostrates polymorphism: one interface, multiple purposes or roles)

Note: For this exercise, do *not* use shorthand/shortcut get/set methods.

    c. Create two constructors:
        1. default constructor (accepts no arguments)
        2. parameterized constructor that accepts six arguments 
    
    d. Instantiate two student objects:
        1. one from default constructor (display default values, not required to modify data member values)
        2. one from paramterized constructor (pass six user-entered arguments to constructor's parameters), display data member values

3. Allow user to press any key to return back to the command line.
#### README.md file should include the following items:

* Screenshot of .NET Core running VALID/INVALID Operation




#### Assignment Screenshots:

*Screenshot of running VALID/INVALID OPERATION*:

![AMPPS Installation Screenshot](img/pic1.png)

![AMPPS Installation Screenshot](img/pic2.png)

![AMPPS Installation Screenshot](img/pic3.png)




