﻿using System;

namespace skillset1
{
    class Program
    {
        static void Main(string[] args)
        {
            string  a1;
			Console.WriteLine("Project skillset2"); 
            Console.WriteLine("Program Requirements:");
            Console.WriteLine("  1) Initialize suitable variable(s);");
            Console.WriteLine("  2) Prompt and capture user input;");
            Console.WriteLine("  3) Display user input;");
            Console.WriteLine("  4) Allow user to press any key to return back to command line.");
            
            Console.Write("\nPlease Enter your full name: ");
            a1 = Console.ReadLine();

            Console.WriteLine("\nHello " + a1 + "!");

            Console.WriteLine("\nPress any key to exit!");
            Console.ReadLine();

        }
    }
}
