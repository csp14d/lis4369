﻿using System;
using System.Text.RegularExpressions;

namespace skillset12
{

    public class Program
    {


        static void Main(string[] args)
        {
            string requirements =
@"////////////////////////////////////////////
Program Requirements:
Using Classes - Inheritance Introduction
    Also, Overloading (Compile-Time Polymorphism) and Overriding (Run-Time Polymorphism)
    Author: Connor Payne
    Create class: Vehicle

Create two private data members:

milesTraveled (float)
gallonsUsed (float)

Create four properties:

Manufacturer (string)
Make (string)
Model (string)
MPG (float)
*Note*: Only Manufacturer, Make, and Model are auto-implemented properties (MPG is read-only)

Create two constructors:
default constructor (accepts no arguments)
parameterized constructor that accepts three arguments

Create two setter/mutator methods:

SetMiles
SetGallons
Create four getter/accessor methods:

GetMiles
GetGallons
GetObjectInfo()
GetObjectInfo(arg) (overloaded method): accepts string arg to be used as a separator for display purposes


Instantiate two vehicle objects:

B) Create Car (derived/sub/child)
    1)Create one private data member (field/instance variable):
    a. Style (string)

    2) Create one property:
    a. Style (string)
    Note: Style property is not auto implemented

    3) Create two constructors:
    a. default constructor (accepts no arguments)
    b. parameterized constructor w/default parameter values that accepts four arguments

    4) Create one getter/accessor method:
    a. GetObjectInfo(arg) (overridden method): accepts string arg to be ised as a separator for display purposes

    5) Instantiate *one* car object:
    a. one from paramterized constructor (passing *all& arguments to its constructor's parameters), display data member valus

C) Must inclue data validation on numeric input

D) Must only allow the following characters as delimiters : , ; :


one from the default constructor (display the default values, then modify and display new data member values)
one from the parameterized constructor passing three arguments to its constructor's parameters), display the data member values
Notes:

E) Allow user to press any key to return back to command line.
/////////////////////////////////////////////////////";



            Console.Write(requirements);

            Console.WriteLine();

            Console.WriteLine("\nvehicle1 - (instanting new object from default constructor)");
            Vehicle vehicle1 = new Vehicle();

            Console.WriteLine(vehicle1.GetObjectInfo());

            Console.WriteLine("\nvehicle2 (user input): Call parameterized base constructor (accepts arguments):");

            string p_manufacturer = "";
            string p_make = "";
            string p_model = "";
            float p_miles = 0.0f;
            float p_gallons = 0.0f;
            string p_separator = "";
            string p_style = "";
            bool delimitLine = false;

            Console.Write("Manufactuer (alpha): ");
            p_manufacturer = Console.ReadLine();

            Console.Write("Make (alpha): ");
            p_make = Console.ReadLine();

            Console.Write("Model (alpha): ");
            p_model = Console.ReadLine();

            Console.Write("Miles driven (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_miles))
            {
                Console.Write("Miles must be numeric: ");
            }

            Console.Write("Gallons used (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_gallons))
            {
                Console.Write("Gallons must be numeric: ");
            }

            Console.WriteLine();

            Console.WriteLine("\nvehicle2 - (instantiating new object, passing *only 1st arg*)\n" + "(Demos why use constructor with default parameter valuse):");
            Console.WriteLine("*Note*: only 1st arg passed to constructor, other are defaut values. ");
            Vehicle vehicle2 = new Vehicle(p_manufacturer);

            Console.WriteLine(vehicle2.GetObjectInfo());

            Console.WriteLine("\nUser input: vehicle2 - passing arg to overloaded GetObjectInfo(arg):");
            


            while (delimitLine == false)
            {
                Console.Write("Delimiter (, : ;): ");
                p_separator = Console.ReadLine();
                if (Regex.IsMatch(p_separator,@"^[,|;|:]$"))
                {

                    delimitLine =true;

                }
                else 
                {
                    delimitLine = false;
                    Console.WriteLine("Enter correct delimiter!!");
                    Console.WriteLine();
                }
            }


        

            Console.WriteLine(vehicle2.GetObjectInfo(p_separator));

            Console.WriteLine();

            Console.WriteLine("\nvehicle3 - (instatiating new object, passing *all* vehicle args):");
            Vehicle vehicle3 = new Vehicle(p_manufacturer, p_make, p_model);

            vehicle3.SetGallons(p_gallons);
            vehicle3.SetMiles(p_miles);

            delimitLine = false;

            Console.WriteLine("\nUser input: vehicle3 - passing arg to overloaded (same scope - same class) GetObjectInfo(arg)");
            while (delimitLine == false)
            {
                Console.Write("Delimiter (, : ;): ");
                p_separator = Console.ReadLine();
                if (Regex.IsMatch(p_separator,@"^[,|;|:]$"))
                {

                    delimitLine =true;
                }
                else 
                {
                    delimitLine = false;
                    Console.WriteLine("Enter correct delimiter!!");
                    Console.WriteLine();
                }
            }
            

            Console.WriteLine(vehicle3.GetObjectInfo(p_separator));

            Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");
            Console.WriteLine("User input: carl - calling parameterized base class constructor explicitly.)");


            Console.Write("\nManufactuer (alpha): ");
            p_manufacturer = Console.ReadLine();

            Console.Write("Make (alpha): ");
            p_make = Console.ReadLine();

            Console.Write("Model (alpha): ");
            p_model = Console.ReadLine();

            Console.Write("Miles driven (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_miles))
            {
                Console.Write("Miles must be numeric: ");
            }

            Console.Write("Gallons used (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_gallons))
            {
                Console.Write("Gallons must be numeric: ");
            }

            Console.Write("Style (alphanumeric): ");
            p_style = Console.ReadLine();


            Console.WriteLine();

            
            Console.WriteLine("\ncar1 -(instantiating new object, passing *only 1st arg*):");
            Car car1 = new Car(p_manufacturer,p_make,p_model,p_style);

            car1.SetGallons(p_gallons);
            car1.SetMiles(p_miles);

            delimitLine = false;
 
            Console.WriteLine("\nUser input: car1 - passing arg to overridden (different scope - inheritance) GetObjectInfo(arg):");
            while (delimitLine == false)
            {
                Console.Write("Delimiter (, : ;): ");
                p_separator = Console.ReadLine();
                if (Regex.IsMatch(p_separator,@"^[,|;|:]$"))
                {
                    delimitLine =true;
                }
                else 
                {
                    delimitLine = false;
                    Console.WriteLine("Enter correct delimiter!!");
                    Console.WriteLine();
                }

            }


            Console.WriteLine(car1.GetObjectInfo(p_separator));

            Console.WriteLine();


            Console.WriteLine();
            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
            


        }
        
    }
    
}
