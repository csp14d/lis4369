﻿using System;

namespace skillset12
{
    public class Car : Vehicle
    {
        private string style;

        public string Style 
        {
            get { return style; }
            set { style = value; }
        }

        public Car()
        {
            this.Style = "Default style";
        
            Console.WriteLine("\nCreating base object from default constructor {accepts no arguments}");
        }


        public Car(string mn = "Manufacturer", string mk = "Make", string md = "Model", string st = "style")
            : base(mn,mk,md)
        {
            this.style = st;

            Console.WriteLine("\nCreating base object from parameterized constructor {accepts arguments}");
        }

        //******Accessor methods ******** */
        //getter methods

        public string GetStyle()
        {
                return style;
        }

    

        public virtual string GetObjectInfo()
        {
            return Manufacturer + " , " + Make + " , " + Model + " , " + MPG + " , " + Style; 
        }

        public virtual string GetObjectInfo(string sep)
        {
            return Manufacturer + sep + Make + sep + Model + sep + MPG + sep + style;
        }

    }
}

