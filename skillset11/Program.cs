﻿using System;

namespace skillset1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string  a1, a2;
            //int entry1, entry2;

            string requirements =
@"//////////////////////////////////
Program Requirements:
Methods: Pass By Value vs Pass By Reference
Author: Connor Payne

 1) Create two methods:
 a. PassByVal
 b. PassByRef

 2) Initialize one (double) variable: num1

 3) Allow user input and data validate.

 4) Pass num1 to each method above.

 5) Allow user to press any key to return back to command line.
 /////////////////////////////////////
";
            
            Console.Write(requirements);
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:sst"));

            double num1 = 0.0;


            Console.Write("\nPlease enter a number ");
            Console.Write("\nnum1: ");
            while (!double.TryParse(Console.ReadLine(), out num1))
            {
                Console.WriteLine("\nnum1 must be numeric.");
                Console.Write("num1: ");
            }

            Console.WriteLine("\nCall PassByVal().");
            PassByVal(num1);
            Console.WriteLine(" In Main() num1 = " + num1);

            Console.WriteLine("\nCall PassByRef().");
            PassByRef(ref num1);
            Console.WriteLine(" In Main() num1 = " + num1);

            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
        }

        public static void PassByVal(double num1)
        {
            num1 += 5;

            Console.WriteLine(" Inside PassByVal() method. ");
            Console.WriteLine(" Added 5 to number passed by value = " + num1);

        }

        public static void PassByRef(ref double num1)
        {
            num1 += 5;

            Console.WriteLine(" Inside PassByRef() method. ");
            Console.WriteLine(" Added 5 to number passed by reference = " + num1);
        }
    }
}
