﻿using System;

namespace skillset1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string  a1, a2;
            //int entry1, entry2;

            string requirements =
            @"Program Requirements:
1) Use intrinsic method to display date/time;
2) Use four types of iteration structures: for, while, do...while, foreach;
3) Initialize suitable variable(s);
4) Loop thorugh numbers 1 -10;
5) Create string array (for days of week);
6) Use foreach iteration structure to loop through days of week;
7) Allow user to press any key to return back to command line.";
            
            Console.Write(requirements);
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\n\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:sst"));


            Console.WriteLine("\nfor loop:");
            for (int a = 1; a < 11; a = a + 1)
            {
                Console.Write("{0} ", a);
            }
            
            Console.WriteLine("\n\nwhile loop:");
            int b = 0;
            while ( b < 10)
            {
                b++;
                Console.Write("{0} ", b);

            }


            Console.WriteLine("\n\ndo...while loop:");
            int c = 1;
                    
             /* do loop execution */
            do
            {
                Console.Write("{0} ", c);
                c = c + 1;
            } 
            while (c < 11);

            Console.WriteLine("\n\nfor each loop:");

            string[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" , "Sunday"};
            foreach (string day in days)
            {
                Console.Write(day);
            }

            Console.WriteLine("\n\nPress any key to exit!");
            Console.ReadKey();
        }
    }
}