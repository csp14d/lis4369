﻿using System;

namespace skillset3
{
    class Program
    {
        static void Main(string[] args)
        {
        

            string requirements =
            @"Program Requirements:
            1) Use intrinsic method to display date/time;
            2) Use two types of selection structures: if...else if...else, and switch;
            3) Initialize suitable variable(s): use decimal data type for bill;
            4) Prompt and capture user input;
            5) Display money in currency format;
            6) Allow user to press any key to return back to command line.";
          

          Console.Write(requirements);
          //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:sst"));

          //First selection method
            Console.WriteLine("\n*****if...else if...else example*****");
            decimal bill = 0.00m;
            
            Console.WriteLine("Your bill is $100.");

            Console.WriteLine("\n\nDiscounts:");
            Console.WriteLine("6+: 30% off");
            Console.WriteLine("3-5: 205 off");
            Console.WriteLine("2: 10% off");
            Console.WriteLine("1: Regular Price");

            Console.Write("\nHow many books did you buy? ");
            int numBooks = 0;
            numBooks = Convert.ToInt16(Console.ReadLine());

            bill = 100.00m;

            if (numBooks >= 2 && numBooks < 3)
                bill = bill * .9m;
            else if (numBooks >= 3 && numBooks < 6)
                bill = bill * .8m;
            else if (numBooks >= 6)
                bill = bill * .7m;
            else 
                bill = bill * 1.0m;

            Console.WriteLine("\nYour cost: {0:C2}", bill);
            //Console.WriteLine("\nYour cost: " + bill);

            Console.WriteLine("\n\n*****switch example*****");
            Console.WriteLine("\n1 - red");
            Console.WriteLine("\n2 - green");
            Console.WriteLine("\n3 - blue");

            Console.Write("\nWhat is your favorite color? ");
            int favColor = 0;
            favColor = Convert.ToInt16(Console.ReadLine());
            

            switch (favColor)
                {
                    case 1:
                        Console.WriteLine("\nYour favorite color is red.");
                        break;
                    case 2:
                        Console.WriteLine("\nYour favorite color is green.");
                        break;
                    case 3:
                        Console.WriteLine("\nYour favorite color is blue.");
                        break;
                    default:
                        Console.WriteLine("\nYou entered an incorrect value.");
                        break;
                }

            Console.Write("\nPress any key to exit!");
            Console.ReadKey();



            
        }
    }
}
