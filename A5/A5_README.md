
# LIS4369

## Connor Payne

### Assignment 5 Requirements:

Author: Connor Payne

1. https://docs.microsoft.com/en-us/aspnet/core/getting-started

	a. dotnet new razor -o a5
	
	b. cd a5 > dotnet run
	
	c. http://localhost:5000/
	
2. Modifications: Make appropriate changes to .cshtml and/or cshtml.cs files below.
	a. Index.cshtml

	b. *About.cshtml* and *About.cshtml.cs*

	c. *Contact.cshtml* and *Contact.cshtml.cs*
		
	d. *favicon.ico*

	e. Footer: Change "© 2017 - a5" to "© 2017 Your Name"





#### Assignment Screenshots:

*Screenshot of running VALID*:

![AMPPS Installation Screenshot](img/main.png)

![AMPPS Installation Screenshot](img/about.png)

![AMPPS Installation Screenshot](img/contact.png)







