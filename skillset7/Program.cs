﻿using System;

namespace p1
{
    class Program
    {
        public static void Main(string[] args)
        {
            string requirements =
@"/////////////////////////////////////////////////
Program Requirements:
P1 - Room Size Calculator using Classes
Author: Connor Payne

/////////////////////////////////////////////////";

            Console.Write(requirements);

            Console.WriteLine("\n\nNow: " + DateTime.Now.ToString("ddd, M/d/yy H:mm:ss t"));
             
            Console.WriteLine("\nCreating person object from default constructor (accepts no arguments:");

            Person person1 = new Person();



            Console.Write("First Name: ");
            Console.WriteLine(person1.GetFname());

            Console.Write("Last Name: ");
            Console.WriteLine(person1.GetLname());






            string Fname = "";
            string Lname = "";



            Console.WriteLine("\nModify default constructor object's data member values:");
            Console.WriteLine("Use setter/getter methods:");
            Console.Write("First Name: ");
            Fname = Console.ReadLine();

            Console.Write("Last Name: ");
            Lname = Console.ReadLine();





            person1.SetFname(Fname);
            person1.SetLname(Lname);
 

            Console.WriteLine("\nDisplay object's new data member values:");

            Console.Write("First Name: ");
            Console.WriteLine(person1.GetFname());

            Console.Write("Last Name: ");
            Console.WriteLine(person1.GetLname());




            Console.WriteLine();






            Console.WriteLine();



            Person person2 = new Person(Fname, Lname);


            Console.WriteLine("Call parameterized constructor (accepts two arguments):");
  
            Console.Write("First Name: ");
            Fname = Console.ReadLine();

            Console.Write("Last Name: ");
            Lname = Console.ReadLine();


            person2.SetFname(Fname);
            person2.SetLname(Lname);


            Console.WriteLine("\nCreating person object from parameterized constructor (accepts two arguments): ");

            Console.Write("Full Name: ");
            Console.WriteLine(person2.GetFullName());


            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();

        }
    }
}