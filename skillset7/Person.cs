﻿using System;

namespace p1
{
    public class Person
    {
        private string type, Fname, Lname, FullName;


        public Person()
        {

            Fname = "John";
            Lname = "Doe";


        }

        public Person( string f ="", string l ="")
        {

            Fname = f;
            Lname = l;


        }


        public void SetFname(string f="")
        {
            Fname = f;
        }

        public void SetLname(string l="")
        {
            Lname = l;
        }

        public void SetFullName(string l="")
        {
            FullName = l;
        }


        public string GetFname()
        {
            return Fname;
        }

        public string GetLname()
        {
            return Lname;
        }

        public string GetFullName()
        {
            return (Fname + " " + Lname);
        }
    }
}