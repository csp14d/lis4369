
# LIS4369

## Connor Payne

### Project 1 Requirements:

*Sub-Heading:*

1. Create Room class:
2. Create following fields (aka properties or data members):
	1. private string type; //room type
	2. private double length; //room length
	3. private double width; //room width
	4. private double height; //room height
3. Create two constructors:
	1. Default constructor
	2. Parameterized constructor that accepts four arguments (for fields above)
4. Create the following mutator (aka setter) methods:
	1. SetType
	2. SetLength
	3. SetWidth
	4. SetHeight
5. Creat the following accessor (aka getter) methods:
	1. GetType
	2. GetLength
	3. GetWidth
	4. GetHeight
	5. GetArea
	6. GetVolume
6. Must include the following functionality:
	1. Display room size calculations in feet (as per below)
	2. Must include data validation
	3. Round to two decimal places
7. Allow user to press any key to return back to command line.

#### README.md file should include the following items:

* Screenshot of .NET Core running Room Calculations




#### Assignment Screenshots:

*Screenshot of running ROOM OPERATION*:

![AMPPS Installation Screenshot](img/rooms.png)







