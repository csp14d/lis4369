﻿using System;

namespace A3
{
    class Program
    {

     public static decimal FutureValue(decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep)
     {
         int numMonths = numYears * 12;
         double monthlyInt = (double)yearlyInt / 12 / 100;

         return presentValue * (decimal)Math.Pow((1 + monthlyInt), numMonths) + (monthlyDep * (decimal)(Math.Pow((1 + monthlyInt), numMonths) -1 ) / (decimal)monthlyInt);

    }




        public static void Main(string[] args)
        {
            Console.Write("\n////////////////////////////////////////////////////////");
            string requirements =
@"
Program Requirements:
A3 - Future Value Calculator 
Author: Connor S Payne
1) Use intrinsic method to display date/time;
2) Research: what is future value? And, its formula;
3) Create FutureValue method using the following parameters: decimal persentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
4) Initialize suitable variables(s): use decimal data types for currency variables;
5) Perform data validation:;
6) Use foreach iteration structure to loop through days of week;
7) Allow user to press any key to return back to command line.";

            Console.Write(requirements);
            Console.WriteLine("\n////////////////////////////////////////////////////////");
            
 
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\n\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:sst"));

            //variables
            decimal fv = 0.0m;
            decimal startingBalance = 0.0m;
            int term = 0;
            decimal interestRate = 0.0m;
            decimal monthlyDeposit = 0.0m;

            //balance
            Console.Write("\nStarting Balance: ");
            while(!Decimal.TryParse(Console.ReadLine(), out startingBalance))
            {
                Console.WriteLine("Starting balance must be numeric.");
                Console.Write("Starting Balance: ");

            }



            //term
            Console.Write("\nTerm (years): ");
            while(!int.TryParse(Console.ReadLine(), out term))
            {
                Console.WriteLine("Term must be integer data type.");
                Console.Write("Term (years): ");

            }


            //interest rate
            Console.Write("\nInterest Rate: ");
            while(!Decimal.TryParse(Console.ReadLine(), out interestRate))
            {
                Console.WriteLine("Interest Rate must be numeric.");
                Console.Write("Interest Rate: ");

            }

            //monthly deposit
            Console.Write("\nDeposit (Monthly): ");
            while(!Decimal.TryParse(Console.ReadLine(), out monthlyDeposit))
            {
                Console.WriteLine("Monthly deposit must be numeric.");
                Console.Write("Deposit (monthly): ");

            }

            fv = FutureValue(startingBalance, term, interestRate, monthlyDeposit);

            Console.WriteLine("\n***Future Value: ***");
            Console.WriteLine(fv.ToString("C2"));

            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
        }
    }
}
