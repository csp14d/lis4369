
# LIS4369

## Connor Payne

### Assignment 3 Requirements:

*Sub-Heading:*

1. Use intrinsic method to display date/time;
2. Research: what is future value? And, its formula;
3. Create FutureValue method using the following parameters: decimal persentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
4. Initialize suitable variables(s): use decimal data types for currency variables;
5. Perform data validation:;
6. Use foreach iteration structure to loop through days of week;
7. Allow user to press any key to return back to command line.

#### README.md file should include the following items:

* Screenshot of .NET Core running VALID/INVALID Operation




#### Assignment Screenshots:

*Screenshot of running VALID/INVALID OPERATION*:

![AMPPS Installation Screenshot](img/running_screenshot.png)







