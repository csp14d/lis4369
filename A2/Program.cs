﻿using System;

namespace skillset1
{
    class Program
    {
        static void Main(string[] args)
        {
            string n1, n2, n3, time;
            decimal entry1, entry2, entry3;
            decimal answer=0;
            
            Console.WriteLine("Project A2 "); 
            Console.WriteLine("///////////////////////////////////////////////");
                Console.WriteLine("A2: Simple Calculator");
                Console.WriteLine("Note: Program does not perform data validation.");
                Console.WriteLine("Connor S. Payne");

            time = DateTime.Now.ToString();
            
            Console.Write("Now:  ");
            Console.WriteLine(time);

            Console.WriteLine("///////////////////////////////////////////////");

            Console.Write("\nnum1: ");
                n1 = Console.ReadLine();
                decimal.TryParse(n1, out entry1);

            Console.Write("num2: ");
                n2 = Console.ReadLine();
                decimal.TryParse(n2, out entry2);
         
            Console.WriteLine("1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplicaiton");
            Console.WriteLine("4 - Division");
            
            Console.Write("\nChoose a mathematical operation: ");
                n3 = Console.ReadLine();
                decimal.TryParse(n3, out entry3);

            Console.WriteLine("*** Result of division operation: ***");
        
            if (entry3 == 1)
            {
                answer = entry1 + entry2;
                Console.WriteLine(answer);
            }

            else if (entry3 == 2)
            {
                answer = entry1 - entry2;
                Console.WriteLine(answer);
            }

            else if (entry3 == 3)
            {
                answer = entry1 * entry2;
                Console.WriteLine(answer);
            }

            else if (entry3 == 4)
            {
                answer = Decimal.Divide(entry1, entry2);
                Console.WriteLine(answer);
            }

            else
            {
                Console.WriteLine("\nAn incorrect mathematical operation was entered.");
            }

            

            Console.WriteLine("\nPress any key to exit!");
            Console.ReadLine();
        }
    }
}
