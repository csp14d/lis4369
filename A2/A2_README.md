
# LIS4369

## Connor Payne

### Assignment 2 Requirements:

*Sub-Heading:*

1. Display short assignment requirements
2. Display *your* name as “author”
3. Display current date/time (must include date/time, your format preference)
4. Must perform and display each mathematical operation (example below)

#### README.md file should include the following items:

* Screenshot of .NET Core running VALID Operation
* Screenshot of .NET Core running INVALID Operation




#### Assignment Screenshots:

*Screenshot of running VALID OPERATION*:

![AMPPS Installation Screenshot](img/valid_operation.png)

*Screenshot of running INVALID OPERATION*:

![JDK Installation Screenshot](img/invalid_operation.png)





