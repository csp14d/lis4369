
# LIS4369

## Connor Payne

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Gitbucket
2. development instructions 
3. Questions
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of hwapp running 
* Screenshot of aspnetcoreapp running My .NET Core installtion 
* git commands with descriptions 




> #### Git commands w/short descriptions:

1. git init - creates an empty git repository 
2. git status - displays the state of the working directory and the staging area
3. git add - adds file contents to the index
4. git commit - adds a change in the working directory to the staging area
5. git push - update remote refs along with associated objects 
6. get pull - fetches all the refs it finds in the remotes 
7. git clone - creates copies of an exisitng git repository 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/local_host.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hello_world.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/csp14d/bitbucketstationlocations "Bitbucket Station Locations")


*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/csp14d/myteamquotes "My Team Quotes Tutorial")

