﻿using System;

namespace skillset5
{
    class Program
    {
        public static void AddNum(double x, double y)
        {
            Console.WriteLine(x + y);
        }
        public static void SubtractNum(double x, double y)
        {
            Console.WriteLine(x - y);
        }
        public static void MultiplyNum(double x, double y)
        {
            Console.WriteLine(x*y);
        }
        public static void DivideNum(double x, double y)
        {
            if (y == 0)
            {
                Console.WriteLine("Cannot divide by zero!");
            }
            else
            {
                Console.WriteLine(x + y);
            }
        }
        public static void PowerNum(double x, double y)
        {
            Console.WriteLine(Math.Pow(x, y));
        }

        static void Main(string[] args)
        {
            
            string requirements =
            @"Title: Non-Value Returning (void) Functions
            Author: Connor Payne";
            
            Console.Write(requirements);
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\n\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:ss t"));

            Console.Write("\nnum1: ");
                double num1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("num2: ");
                double num2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nChoose a mathematical operation: ");

            Console.WriteLine("\n1 - Addition");
            Console.WriteLine("2 - Subtraction");
            Console.WriteLine("3 - Multiplicaiton");
            Console.WriteLine("4 - Division");
            Console.WriteLine("5 - Exponentiation");

            int oper = 0;
            oper = Convert.ToInt16(Console.ReadLine());

            switch(oper)
            {
                case 1:
                Console.WriteLine("\n*** Result of Addition Operation: ***");
                AddNum(num1, num2);
                    break;
                
                case 2:
                Console.WriteLine("\n*** Result of Subtraction Operation: ***");
                SubtractNum(num1, num2);
                    break;  
                case 3:
                Console.WriteLine("\n*** Result of Multiplication Operation: ***");
                MultiplyNum(num1, num2);
                    break;

                case 4:
                Console.WriteLine("\n*** Result of Division Operation: ***");
                DivideNum(num1, num2);
                    break;

                case 5:
                Console.WriteLine("\n*** Result of Power Operation: ***");
                PowerNum(num1, num2);
                    break;
            }
            
            Console.WriteLine("\nPress Enter to exit...");
            Console.ReadKey();
        }
    }
}
