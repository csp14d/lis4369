﻿using System;

namespace skillset10
{
    class Program
    {
        static void Main(string[] args)
        {
            string requirements =
@"////////////////////////////////////////////
Program Requirements:
Using Classes - Inheritance Introduction
    Also, Overloading (Compile-Time Polymorphism) and Overriding (Run-Time Polymorphism)
    Author: Connor Payne
    Create class: Vehicle

Create two private data members:

milesTraveled (float)
gallonsUsed (float)

Create four properties:

Manufacturer (string)
Make (string)
Model (string)
MPG (float)
*Note*: Only Manufacturer, Make, and Model are auto-implemented properties (MPG is read-only)

Create two setter/mutator methods:

SetMiles
SetGallons
Create four getter/accessor methods:

GetMiles
GetGallons
GetObjectInfo()
GetObjectInfo(arg) (overloaded method): accepts string arg to be used as a separator for display purposes

Create two constructors:

default constructor (accepts no arguments)
parameterized constructor that accepts three arguments
Instantiate two vehicle objects:

one from the default constructor (display the default values, then modify and display new data member values)
one from the parameterized constructor passing three arguments to its constructor's parameters), display the data member values
Notes:

- *Must* include data validation on numeric input.
- Allow user to press any key to return back to command line.";



            Console.Write(requirements);

            Console.WriteLine();

            Console.WriteLine("\nvehicle1 - (instanting new object from default constructor)");
            Vehicle vehicle1 = new Vehicle();

            Console.WriteLine(vehicle1.GetObjectInfo());

            Console.WriteLine("\nvehicle2 (user input): Call parameterized base constructor (accepts arguments):");

            string p_manufacturer = "";
            string p_make = "";
            string p_model = "";
            float p_miles = 0.0f;
            float p_gallons = 0.0f;
            string p_separator = "";

            Console.Write("Manufacture (alpha): ");
            p_manufacturer = Console.ReadLine();

            Console.Write("Make (alpha): ");
            p_make = Console.ReadLine();

            Console.Write("Model (alpha): ");
            p_model = Console.ReadLine();

            Console.Write("Miles driven (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_miles))
            {
                Console.Write("Miles must be numeric: ");
            }

            Console.Write("Gallons used (float): ");
            while (!float.TryParse(Console.ReadLine(), out p_gallons))
            {
                Console.Write("Gallons must be numeric: ");
            }

            Console.WriteLine("\nvehicle2 - (instantiating new object, passing *only 1st arg*)\n" + "(Demos why use constructor with default parameter valuse):");
            Console.WriteLine("*Note*: only 1st arg passed to constructor, other are defaut values. ");
            Vehicle vehicle2 = new Vehicle(p_manufacturer);

            Console.WriteLine(vehicle2.GetObjectInfo());

            Console.WriteLine("\nUser input: vehicle2 - passing arg to overloaded GetObjectInfo(arg):");
            Console.Write("Delimiter (, : ;): ");

            p_separator = Console.ReadLine();

            Console.WriteLine(vehicle2.GetObjectInfo(p_separator));

            Console.WriteLine("\nvehicle3 - (instatiating new objet, passing *all* vehicle args):");
            Vehicle vehicle3 = new Vehicle(p_manufacturer, p_make, p_model);

            vehicle3.SetGallons(p_gallons);
            vehicle3.SetMiles(p_miles);

            Console.WriteLine("\nUser input: vehicle2 - passing arg to overloaded (same scope - same class) GetObjectInfo(arg)");
            Console.Write("Delimiter (, : ;): ");

            p_separator = Console.ReadLine();

            Console.WriteLine(vehicle3.GetObjectInfo(p_separator));

            Console.WriteLine();
            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
            


        }
    }
}
