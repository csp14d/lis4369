﻿using System;

namespace skillset1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string  a1, a2;
            //int entry1, entry2;

            string requirements =
            @"Program Requirements:
            1) Use intrinsic method to display date/time;
            2) Use four types of iteration structures: for, while, do...while, foreach;
            3) Initialize suitable variable(s);
            4) Loop thorugh numbers 1 -10;
            5) Create string array (for days of week);
            6) Use foreach iteration structure to loop through days of week;
            7) Allow user to press any key to return back to command line.";
            
            Console.Write(requirements);
             //Write Date
            DateTime now = DateTime.Now;
            Console.WriteLine("\nNow: " + now.ToString("ddd, MM/dd/yy hh:mm:sst"));



            Console.WriteLine("\nPress any key to exit!");
            Console.ReadLine();
        }
    }
}
