# LIS4369

## Connor Payne

 1. [A1 README.md](A1/A1_README.md "My A1 README.md file")

	* Install .NET core
	* Create hwapp application
	* create aspnetcoreapp application
	* Provide screenshots of installtions 
	* Create Bitbucket repo
	* Complete Bitbucket repo 
	* Complete Bitbucket tutorials 
	* Provide git command descriptions 


 2. [A2 README.md](A2/A2_README.md "My A2 README.md file")

	* Screenshot of .NET Core running VALID Operation
	* Screenshot of .NET Core running INVALID Operation
	
	
 3. [A3 README.md](A3/A3_README.md "My A3 README.md file")
 
	* Use intrinsic method to display date/time;
	* Research: what is future value? And, its formula;
	* Create FutureValue method using the following parameters: decimal persentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
	* Initialize suitable variables(s): use decimal data types for currency variables;
	* Perform data validation:;
	* Use foreach iteration structure to loop through days of week;
	* Allow user to press any key to return back to command line.
	

 4. [P1 README.md](P1/P1_README.md "My P1 README.md file")
 
	* Backward-engineer (using .NET Core) the following console application screenshot:
	* Requirements (see below screenshot):
	* Display short assignment requirements
	* Display *your* name as “author”
	* Display current date/time (must include date/time, your format preference)
	* Must perform and display room size calculations, must include data validation and rounding to two decimal places.
	* Each data member must have get/set methods, also GetArea and GetVolume

 5. [A4 README.md](A4/A4_README.md "My A4 README.md file")
 

	* Display short assignment requirements.
	* Display *your* name as “author.”
	* Display current date/time (must include date/time, your format preference).
	* Create two classes: person and student (see fields and methods below).	
	* Must include data validation on numeric data.
	
	
 6. [A5 README.md](A5/A5_README.md "My A5 README.md file")
 
 	* Make necessary changes to webpages. 
	* Display *your* name as “author.”
	* Display current date/time (must include date/time, your format preference).
	
 7. [P2 README.md](P2/P2_README.md "My P2 README.md file")
 
	* Prompt user for last name. Return full name, occupation, and age.
	* Prompt user for ace and occupation (Dev or Manager). Return full name.
	* Allow user to press any key to return back to command line.